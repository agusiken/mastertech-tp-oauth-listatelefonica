package br.com.mastertech.contato.repository;

import br.com.mastertech.contato.model.Contato;
import org.springframework.data.repository.CrudRepository;

public interface ContatoRepository extends CrudRepository<Contato, Integer> {
    Iterable<Contato> findAllByIdUsuario(Integer id);
}
