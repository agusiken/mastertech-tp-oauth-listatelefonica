package br.com.mastertech.contato.controller;

import br.com.mastertech.contato.model.Contato;
import br.com.mastertech.contato.security.Usuario;
import br.com.mastertech.contato.service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/contato")
public class ContatoController {

    @Autowired
    private ContatoService contatoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Contato postContato(@RequestBody Contato contato, @AuthenticationPrincipal Usuario usuario){
        contato.setIdUsuario(usuario.getId());
        Contato contatoObjeto = contatoService.salvarContato(contato);

        return contatoObjeto;
    }

    @GetMapping
    public Iterable<Contato> getAllByIdUsuario(@AuthenticationPrincipal Usuario usuario) {
        Iterable<Contato> contatoObjeto = contatoService.getAllByIdUsuario(usuario.getId());
        return contatoObjeto;

    }
}
