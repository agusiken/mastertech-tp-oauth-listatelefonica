package br.com.mastertech.contato.service;

import br.com.mastertech.contato.model.Contato;
import br.com.mastertech.contato.repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository contatoRepository;

    public Contato salvarContato(Contato contato){
        
        return contatoRepository.save(contato);
    }

    public Iterable<Contato> findAll(){
        Iterable<Contato> contatos = contatoRepository.findAll();
        return contatos;
    }

    public Iterable<Contato> getAllByIdUsuario(int id) {
        Iterable<Contato> contatos = contatoRepository.findAllByIdUsuario(id);
        return contatos;
    }
}
