# EXERCÍCIO UTILIZANDO OAUTH 
**Módulo de especialização 3 - Mastertech**

Crie um microsserviço que guarde contatos telefônicos dos usuários.

Quando o usuário chamar POST /contato, ele deve criar um contato para o usuário logado.
Quando o usuário chamar GET /contato, ele deve listar todos os contatos deste usuário.

Utilização: 
- Spring Boot - Maven
- Spring Web
- JPA
- H2
- Oauth 2.0 Cloud



